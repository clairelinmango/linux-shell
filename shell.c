#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define MAXLINE 80        /* The maximum length command */

int main(void)
{
	  char *args[MAXLINE/2 + 1];  /* command line with max 40 arguments */
	  char str[80];               /*to store command line input*/
	  int should_not_wait =0;     /*flag to determine if input contains "&" */
	  int should_run = 1;         /* flag to determine when to exit program */
	  printf("CS149 Shell from Claire Lin\n");
	  while (should_run)
	  {
	    printf("Claire-493>");
	    fflush(stdout);
	    scanf("%[^\n]%*c", str);      /* reads command line until \n and clears input stream */
        should_not_wait =0;
	    char* token = strtok(str," ");/*parse command line */
	   	int i=0;
	   	while (token!= NULL)
	   	{
	   	     args[i] = token;
	   	     token = strtok(NULL," ");
	   	     i++;
	   	}
	   	args[i] = NULL;    /* set last element string token to NULL */
	   	if (strcmp(args[0],"exit")==0)  /*exit while loop when input is exit*/
	    {
	   		  should_run=0;
	   		  exit(0);
	    }
	   	else if (strcmp(args[i-1],"&")==0)  /* set flag to not call wait() and clear "&" in args */
	   	{
	   		 should_not_wait=1;
	   		 args[i-1]= NULL;
	   	}
	   	pid_t pid = fork();   /* fork parent process */
	   	if (pid <0 )    /* if fork fails */
	   	{
	   	    printf("Fork error \n");
	   	    exit(1);
	   	 }
	    else if (pid==0)   /* if fork succeeds */
	    {
	   	    execvp(args[0], args);
	   	    printf("Exec error: invalid command. \n");
	   	    for(int j=0;j<i;j++)   /*clear string token array */
	   	    {
	   	    	args[j] = NULL;
	   	    }
	   	    exit(1);
	   	 }
	   	 else if (should_not_wait!=1) /* if command line doesn't include "&" */
	   	 {
	   		 while(pid!=wait(NULL));
	   	 }
	  }
	  return 0;
}
